import kotlinx.coroutines.*
import kotlinx.coroutines.channels.*
import java.util.Collections.synchronizedSet
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.HashSet
import kotlin.concurrent.thread
import kotlin.concurrent.timer
import kotlin.concurrent.timerTask
import kotlin.random.Random

typealias FunctionalUnitId = Int

data class Uow(val id: Int, val functionalUnits: Collection<FunctionalUnitId>, val operation: String)

val rnd = Random(10)
val uowCounter = AtomicInteger(0)
val generateNewUows = AtomicBoolean(true)

// suspending FIFO
val inputChannel = Channel<Uow>(Channel.UNLIMITED)

fun main(argv: Array<String>) {
    // generate some "traffic"
    val threads = (0..1).map { i ->
        thread {
            while (generateNewUows.get()) {
                try {
                    val newUow = Uow(uowCounter.incrementAndGet(), listOf(i), "foo")
                    inputChannel.sendBlocking(newUow)
                    println("New UOW: ${newUow.id}")
                } catch (ex: ClosedSendChannelException) {
                    break
                }

                Thread.sleep(5000 + rnd.nextLong(2000))
            }
        }
    }.toList()


    // no new UOWs after 30s
    timer(initialDelay = 30000, period = 1000) {
        generateNewUows.set(false)
    }

    // process incoming UOW
    GlobalScope.launch {
        val busyFusSet = synchronizedSet(HashSet<FunctionalUnitId>())
        val postponedUows = synchronizedSet(HashSet<Uow>())

        val releaseEvent = Channel<Set<FunctionalUnitId>>()
        val postponeEvent = Channel<Set<Uow>>()

        val mainContext = this.coroutineContext

        fun canExecute (uow: Uow): Boolean = !uow.functionalUnits.any { it in busyFusSet }

        // whenever a FU is released, review the delayed UOWs
        launch {
            for (s in releaseEvent) {
                val toAdd = postponedUows.filter { canExecute(it) }.toList()

                for (uow in toAdd) {
                    println("Readding: $uow")
                    postponedUows.remove(uow)
                    postponeEvent.send(postponedUows)
                    inputChannel.send(uow)
                }
            }
        }

        // print the delayed UOWs
        launch {
            for (ev in postponeEvent) {
                println("Delayed UOWs: ${ev.map { it.id }}")
            }
        }

        // process incoming UOWs
        for (uow in inputChannel) {
            println("Got UOW: $uow")
            println("BusyFus: $busyFusSet")

            if (canExecute(uow)) {
                println("Allocating FUs ${uow.functionalUnits}")
                busyFusSet += uow.functionalUnits

                launch {
                    println("EXECUTING UOW: $uow")
                    // very heavy FU work
                    delay(20000)
                }.invokeOnCompletion {
                    launch(mainContext) {
                        println("Freeing ${uow.functionalUnits}")
                        busyFusSet -= uow.functionalUnits
                        releaseEvent.send(busyFusSet)
                    }
                }
            } else {
                println("Conflict!!!")
                postponedUows.add(uow)
                postponeEvent.send(postponedUows)
            }
        }
    }
}
